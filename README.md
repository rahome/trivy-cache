# trivy-cache

> [trivy](https://github.com/aquasecurity/trivy) is a simple and comprehensive
vulnerability scanner for containers and other artifacts.

The purpose of this project is to improve pipeline performance when scanning
for vulnerabilities in containers by providing a prebuilt Docker container
where the vulnerability database is already downloaded.

The Docker container is rebuilt automatically on a schedule which will also
update the vulnerability database, you can view the schedule configuration in
the [Schedules](https://gitlab.com/rahome/trivy-cache/-/pipeline_schedules)
view.

## Use with GitLab CI

If you are using GitLab CI to build your CI/CD pipelines, you can include the
[`Trivy.gitlab-ci.yml`](Trivy.gitlab-ci.yml) job template which exposes jobs to
scan the project container(s) for vulnerabilities.

### Include job template

How you include the job template is depending on whether you use GitLab.com or
a self-hosted instance.

#### Using GitLab.com

If you are using GitLab.com you can use the `project:` directive since both
projects are hosted on GitLab.com.

```yaml
include:
  - project: 'rahome/trivy-cache'
    ref: main
    file: '/Trivy.gitlab-ci.yml'
```

#### Using self-hosted instance

If you use a self-hosted instance you need to use the `remote:` directive
since the project can not be resolved using a relative path.

```yaml
include:
  - remote: 'https://gitlab.com/rahome/rahome/trivy-cache/-/raw/main/Trivy.gitlab-ci.yml'
```

If you want to use the `project:` directive on a self-hosted instance, you
can configure a repository mirror from this project to a project on your
self-hosted instance.

However, it is important to know that you will still use the image from this
project as the `image:` directive include the container registry path (i.e.
`registry.gitlab.com`). This is required to prevent supply chain attacks, i.e.
where another registry supply a malicious image with the same name.

#### Version considerations

Since we are unable to track who is using the template and we might need to
do breaking changes, it is recommended to not use a branch reference (i.e.
`main` in this case) instead you should use a git commit hash.

It might make it a bit harder to keep up-to-date, but your pipelines will be
better isolated from changes.

### Pipeline considerations

As the purpose of the project is to scan containers, it is recommended to run
the scan job **after** you have pushed your image to the registry. Also, it is
recommended to declare a dependency on the push to registry job from the scan
job.

The [`.gitlab-ci.yml`](.gitlab-ci.yml) in this project contain an example for
how this can be done.

### Continuous scanning

The `trivy-cache` container is recreated, with a fresh database, once per day
at 06:00 Europe/Stockholm via a scheduled GitLab CI job.

In order to enable continuous scanning it's recommended to schedule jobs after
06:30 Europe/Stockholm, the recreate job can take a few minutes to run.
