FROM aquasec/trivy:0.60.0
LABEL maintainer="Tobias Raatiniemi <raatiniemi@gmail.com>"

ENV TRIVY_CACHE_DIR /var/cache/trivy

RUN set -x \
  && addgroup -g 10001 -S trivy \
  && adduser -u 10001 -H -G trivy -S trivy \
  && trivy clean --all \
  && trivy image --download-db-only --no-progress \
  && trivy image --download-java-db-only --no-progress \
  && trivy --version \
  && chown -R trivy:trivy /var/cache/trivy

USER trivy:trivy

ENTRYPOINT [""]
